
let koronavenn2 = builtins.fetchGit {
      url = "git@gitlab.com:ask81/koronavenn";
      ref = "master";
      rev =  "b2cba7e5d709f4a313a18f7e008eff61a65743e8";
      };

    stadler_no = builtins.fetchGit {
    url = "git@gitlab.com:ask81/stadler_no.git";
    ref = "refs/heads/master";
    rev =  "2dd00af8a7ac4cfcc95c40ab35d197d53e60e44b";
    };

in
    let backend= { config, pkgs, ... }: {

  	environment.systemPackages = with pkgs; [cacert git vim ];
  	
  	nixpkgs.config.tarball-ttl = 0;

  	users.users.root = {
    	openssh.authorizedKeys.keys = [
      	"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAII31N86Na4kQXkE9CUWMFHInRCofAa/TBVFk36Hpov/B ask@nixos"
    	"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILcVA7g76+dVpKpo9BnrbmkHkaXPBN5a0qGtDMgLlFBi aksel@stadler.no"
    	];
  	};

        services.openssh.enable = true;
  	services.openssh.permitRootLogin = "yes";
  	imports = [
      	"${koronavenn2.outPath}/service.nix"
      	"${stadler_no.outPath}/service.nix"

  	];
        networking.firewall.allowedTCPPorts = [ 22 4343 8080 80 4000 443 4001 4002];
  	services.koronavenn.enable = true;
  	services.stadler_no.enable = true;
  	services.koronavenn.port= 4001;
  	services.stadler_no.port= 4002;

	systemd.tmpfiles.rules = [
  	"d /root/tmp 0755 root root"
	];



        services.nginx = {
            enable = true;
            recommendedProxySettings = true;
            recommendedTlsSettings = true;
            # other Nginx options

            virtualHosts."koronavenn.no" =  {
              enableACME = true;
              forceSSL = true;
              locations."/" = {
                proxyPass = "http://127.0.0.1:4001";
                proxyWebsockets = true; # needed if you need to use WebSocket
              };
            };

            virtualHosts."www.koronavenn.no" =  {
              enableACME = true;
              forceSSL = true;
              locations."/" = {
                proxyPass = "http://127.0.0.1:4001";
                proxyWebsockets = true; # needed if you need to use WebSocket
              };
            };

            virtualHosts."stadler.no" =  {
              enableACME = true;
              forceSSL = true;
              locations."/" = {
                proxyPass = "http://127.0.0.1:4002";
                proxyWebsockets = true; # needed if you need to use WebSocket
              };
            };

        };
#
        security.acme.acceptTerms = true;
	security.acme.certs = {
  	"koronavenn.no".email = "aksel@stadler.no";
  	"www.koronavenn.no".email = "aksel@stadler.no";
  	"stadler.no".email = "aksel@stadler.no";
	};
#
    	};


in

{
  network.description = "Test server";
  asknixops1= backend;
}

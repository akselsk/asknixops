

  # Hardware config
 let machine = { config, pkgs, ... }: {
    deployment.targetEnv = "digitalOcean";
    deployment.digitalOcean.enableIpv6 = true;
    deployment.digitalOcean.region = "ams3";
    deployment.digitalOcean.size = "s-2vcpu-4gb";
    deployment.digitalOcean.vpc_uuid= "default-ams3";
   # deployment.digitalOcean.authToken = "Doesn't seem to work";
  };
in
{
  resources.sshKeyPairs.ssh-key = {
    publicKey = builtins.readFile ./tstKey.pub;
    privateKey = builtins.readFile ./tstKey;
  };

  asknixops1= machine;

}
